
#ifndef _TIME_STAMP_H
#define _TIME_STAMP_H



#include <ctime>
#include <cmath>
#include <cstdint>
#include <iostream>




class TimeStamp
{
protected:
    uint32_t sec_;
    uint32_t nsec_;
    bool negative_;

public:
    TimeStamp() :
        TimeStamp(0, 0, false)
    {
        return;
    }

    TimeStamp(uint32_t sec, uint32_t nsec, bool negative=false) :
        sec_(sec),
        nsec_(nsec),
        negative_(negative)
    {
        return;
    }

    TimeStamp(const TimeStamp* t) :
        TimeStamp(t->sec_, t->nsec_, t->negative_)
    {
        return;
    }

    TimeStamp(double time_in_secs)
    {
        double time_in_secs_abs=std::abs(time_in_secs);
        if(time_in_secs>=0)
        {
            this->negative_=false;
        }
        else
        {
            this->negative_=true;
        }
        double fract_part, int_part;
        fract_part = modf (time_in_secs_abs , &int_part);

        this->sec_=static_cast<uint32_t>(int_part);
        this->nsec_=static_cast<uint32_t>(fract_part*1e9);

        return;
    }

public:
    double getDouble() const
    {
        return this->getSign()*static_cast<double>(this->sec_)+1e-9*static_cast<double>(this->nsec_);
    }
    uint32_t getSec() const
    {
        return this->sec_;   
    }
    uint32_t getNSec() const
    {
        return this->nsec_;
    }
    int getSign() const
    {
        if(this->negative_==false)
            return 1;
        else
            return -1;
    }

public:
    int toNSec() const
    {
        int nsec;
        nsec=this->getSign()*static_cast<int>(this->sec_)*1e9 + static_cast<int>(this->nsec_);
        return nsec;
    }

    // Boolean sign
public:
    bool isZero() const
    {
        if(this->sec_ == 0 && this->nsec_ == 0)
            return true;
        else
            return false;
    }

    bool isNegative() const
    {
        return this->negative_;
    }

    bool isPositive() const
    {
        return !this->negative_;
    }

    bool isStrictlyNegative() const
    {
        if(this->isZero())
            return false;
        else
            return this->negative_;
    }

    bool isStrictlyPositive() const
    {
        if(this->isZero())
            return false;
        else
            return !this->negative_;
    }


    // Logic operators
public:
    bool operator==(const TimeStamp& t2) const
    {
        if(this->negative_ == t2.negative_ && this->sec_ == t2.sec_ && this->nsec_ == t2.nsec_)
            return true;
        else
            return false;
    }

    bool operator!=(const TimeStamp& t2) const
    {
        if(this->operator==(t2))
            return false;
        else
            return true;
    }

    // TODO FIX!
    bool operator>(const TimeStamp& t2) const
    {
        if(!this->isNegative() && t2.isNegative())
            return true;

        if(this->isNegative() && !t2.isNegative())
            return false;

        if(!this->isNegative() && !t2.isNegative())
        {
            if(this->sec_ > t2.sec_)
                return true;
            else if(this->sec_ == t2.sec_ && this->nsec_ > t2.nsec_)
                return true;
            else
                return false;
        }

        if(this->isNegative() && t2.isNegative())
        {
            //TODO
            std::cerr<<"TimeStamp::operator>() functionality not implemented!"<<std::endl;
            throw 1;
        }
    }

    bool operator>=(const TimeStamp& t2) const
    {
        if(this->operator>(t2) || this->operator==(t2))
            return true;
        else
            return false;
    }

    bool operator<(const TimeStamp& t2) const
    {
        if(this->operator>=(t2))
            return false;
        else
            return true;
    }

    bool operator<=(const TimeStamp& t2) const
    {
        if(this->operator>(t2))
            return false;
        else
            return true;
    }
    
    // Aritmetic operators
public:
    // tr = t1 - t2
    // TODO finish
    TimeStamp operator-(const TimeStamp& t2) const
    {
        TimeStamp time_diff;

        if(!this->isNegative() && !t2.isNegative())
        {
            if(this->operator<(t2))
            {
                // t2 > t1 -> tr < 0
                time_diff=t2-this;
                time_diff.negative_=true;

                return time_diff;
            }
            else
            {
                // t2 < t1 -> tr > 0
                TimeStamp aux_time_stamp(this);
                if(this->nsec_<t2.nsec_)
                {
                    // nsec
                    aux_time_stamp.nsec_+=1e9;
                    // sec
                    aux_time_stamp.sec_-=1;
                }
                // nsec
                time_diff.nsec_=aux_time_stamp.nsec_-t2.nsec_;
                //sec
                time_diff.sec_=aux_time_stamp.sec_-t2.sec_;
                //negative
                time_diff.negative_=false;

                return time_diff;
            }
        }

        if(!this->isNegative() && t2.isNegative())
        {
            // TODO
            std::cerr<<"TimeStamp::operator-() functionality not implemented!"<<std::endl;
            throw 1;
        }

        if(this->isNegative() && !t2.isNegative())
        {
            // TODO
            std::cerr<<"TimeStamp::operator-() functionality not implemented!"<<std::endl;
            throw 1;
        }

        if(this->isNegative() && t2.isNegative())
        {
            // TODO
            std::cerr<<"TimeStamp::operator-() functionality not implemented!"<<std::endl;
            throw 1;
        }

    }
    
    // tr = t1 + t2
    // TODO finish
    TimeStamp operator+(const TimeStamp& t2) const
    {
        TimeStamp time_add;

        if(!this->isNegative() && !t2.isNegative())
        {
            // nsec
            time_add.nsec_=this->nsec_+t2.nsec_;
            // sec
            time_add.sec_=this->sec_+t2.sec_;
            // negative
            time_add.negative_=false;

            // Fix nsec
            if(time_add.nsec_>1e9)
            {
                // nsec
                time_add.nsec_-=1e9;
                // sec
                time_add.sec_+=1;
            }

            return time_add;
        }

        if(!this->isNegative() && t2.isNegative())
        {
            // TODO
            std::cerr<<"TimeStamp::operator+() functionality not implemented!"<<std::endl;
            throw 1;
        }

        if(this->isNegative() && !t2.isNegative())
        {
            // TODO
            std::cerr<<"TimeStamp::operator+() functionality not implemented!"<<std::endl;
            throw 1;
        }

        if(this->isNegative() && t2.isNegative())
        {
            // TODO
            std::cerr<<"TimeStamp::operator+() functionality not implemented!"<<std::endl;
            throw 1;
        }

    }
    
};




#endif
